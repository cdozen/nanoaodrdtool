./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018A_v2/20000 Run2018A1.root &> Run2018A1.out &
./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018A_v2/250000 Run2018A2.root &> Run2018A2.out &
./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018A_v2/40000 Run2018A3.root &> Run2018A3.out &
./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018A_v2/60000 Run2018A4.root &> Run2018A4.out &
./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018B_v2 Run2018B.root &> Run2018B.out &
./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018C_v2 Run2018C.root &> Run2018C.out &
./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018D_v2/100000 Run2018D1.root &> Run2018D1.out &
./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018D_v2/270000 Run2018D2.root &> Run2018D2.out &
./processnanoaod.py --allinone --json=data/Cert_314472-325175_13TeV_17SeptEarlyReReco2018ABC_PromptEraD_Collisions18_JSON.txt /data/users/sarakm0704/nanoaod-processed/2018/RD/Run2018D_v2/70000 Run2018D3.root &> Run2018D3.out &

./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/W1JetsToLNu WToLNu_1J.root &> WToLNu_1J.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/W2JetsToLNu WToLNu_2J.root &> WToLNu_2J.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/W3JetsToLNu WToLNu_3J.root &> WToLNu_3J.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/W4JetsToLNu WToLNu_4J.root &> WToLNu_4J.out &

./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/TTTo2L2Nu_v4/230000 TTTo2L2Nu1.root &> TTTo2L2Nu1.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/TTTo2L2Nu_v4/240000 TTTo2L2Nu2.root &> TTTo2L2Nu2.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/TTTo2L2Nu_v4/60000 TTTo2L2Nu3.root &> TTTo2L2Nu3.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/TTToSemiLeptonic_v4/250000 TTToSemiLeptonic1.root &> TTToSemiLeptonic1.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/TTToSemiLeptonic_v4/260000 TTToSemiLeptonic2.root &> TTToSemiLeptonic2.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/TTToSemiLeptonic_v4/60000 TTToSemiLeptonic3.root &> TTToSemiLeptonic3.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/TTToHadronic_v4 TTToHadronic.root &> TTToHadronic.out &

./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/DYJetsToLL_M-50 DYJetsToLL_M-50.root &> DYJetsToLL_M-50.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/DYJetsToLL_M-10to50 DYJetsToLL_M-10to50.root &> DYJetsToLL_M-10to50.out &

./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/ST_t-channel_antitop ST_t-channel_antitop.root &> ST_t-channel_antitop.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/ST_t-channel_top ST_t-channel_top.root &> ST_t-channel_top.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/ST_tW_top ST_tW_top.root &> ST_tW_top.out &
./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/MC/ST_tW_antitop ST_tW_antitop.root &> ST_tW_antitop.out &

./processnanoaod.py --allinone /data/users/sarakm0704/nanoaod-processed/2018/LQ LQ_signal.root &> LQ_signal.out &
