#!/bin/bash
centraldata_dir="/data/common/NanoAOD/data"
centralmc16_dir="/data/common/NanoAOD/mc/RunIISummer16NanoAODv6"
centralmc17_dir="/data/common/NanoAOD/mc/RunIIFall17NanoAODv6"
centralmc18_dir="/data/common/NanoAOD/mc/RunIIAutumn18NanoAODv6"

targetmc16_dir="/data1/common/skimmed_NanoAOD/skim_NDFv3_6_0/mc/2016"
targetmc17_dir="/data1/common/skimmed_NanoAOD/skim_NDFv3_6_0/mc/2017"
targetmc18_dir="/data1/common/skimmed_NanoAOD/skim_NDFv3_6_0/mc/2018"

#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/DYJetsToLL_M-10to50  > DYJetsToLL_M-10to50.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8 $targetmc18_dir/DYJetsToLL_M-50-amcatnloFXFX  > DYJetsToLL_M-50-amcatnloFXFX.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-15to20_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-15to20_MuEnrichedPt5  > QCD_Pt-15to20_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-20to30_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-20to30_MuEnrichedPt5  > QCD_Pt-20to30_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-30to50_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-30to50_MuEnrichedPt5  > QCD_Pt-30to50_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-50to80_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-50to80_MuEnrichedPt5  > QCD_Pt-50to80_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-80to120_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-80to120_MuEnrichedPt5  > QCD_Pt-80to120_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-120to170_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-120to170_MuEnrichedPt5  > QCD_Pt-120to170_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-170to300_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-170to300_MuEnrichedPt5  > QCD_Pt-170to300_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-300to470_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-300to470_MuEnrichedPt5  > CD_Pt-300to470_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-470to600_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-470to600_MuEnrichedPt5  > QCD_Pt-470to600_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-600to800_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-600to800_MuEnrichedPt5  > QCD_Pt-600to800_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-800to1000_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-800to1000_MuEnrichedPt5  > QCD_Pt-800to1000_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/QCD_Pt-1000toInf_MuEnrichedPt5_TuneCP5_13TeV_pythia8 $targetmc18_dir/QCD_Pt-1000toInf_MuEnrichedPt5  > QCD_Pt-1000toInf_MuEnrichedPt5.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 $targetmc18_dir/ST_t-channel_antitop  > ST_t-channel_antitop.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/ST_t-channel_top_4f_InclusiveDecays_TuneCP5_13TeV-powheg-madspin-pythia8 $targetmc18_dir/ST_t-channel_top  > ST_t-channel_top.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8 $targetmc18_dir/ST_tW_antitop  > ST_tW_antitop.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8 $targetmc18_dir/ST_tW_top  > ST_tW_top.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8 $targetmc18_dir/TTTo2L2Nu  > TTTo2L2Nu.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/TTWJetsToLNu_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8 $targetmc18_dir/TTWJetsToLNu  > TTWJetsToLNu.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8 $targetmc18_dir/TTWJetsToQQ  > TTWJetsToQQ.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/TTZToLLNuNu_M-10_TuneCP5_13TeV-amcatnlo-pythia8 $targetmc18_dir/TTZToLLNuNu_M-10  > TTZToLLNuNu_M-10.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8 $targetmc18_dir/TTZToQQ_TuneCP5_13TeV  > TTZToQQ_TuneCP5_13TeV.out

#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/TTToHadronic_TuneCP5_13TeV-powheg-pythia8 $targetmc18_dir/TTToHadronic  > TTToHadronic.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8 $targetmc18_dir/TTToSemiLeptonic  > TTToSemiLeptonic.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WJetsToLNu_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/WJetsToLNu  > WJetsToLNu.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WJetsToLNu_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/WJetsToLNu_HT-100To200  > WJetsToLNu_HT-100To200.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WJetsToLNu_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/WJetsToLNu_HT-200To400  > WJetsToLNu_HT-200To400.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WJetsToLNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/WJetsToLNu_HT-400To600  > WJetsToLNu_HT-400To600.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WJetsToLNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/WJetsToLNu_HT-600To800  > WJetsToLNu_HT-600To800.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WJetsToLNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/WJetsToLNu_HT-800To1200  > WJetsToLNu_HT-800To1200.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WJetsToLNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/WJetsToLNu_HT-1200To2500  > WJetsToLNu_HT-1200To2500.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WJetsToLNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc18_dir/WJetsToLNu_HT-2500ToInf  > WJetsToLNu_HT-2500ToInf.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WWTo2L2Nu_NNPDF31_TuneCP5_13TeV-powheg-pythia8 $targetmc18_dir/WWTo2L2Nu  > WWTo2L2Nu.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WWToLNuQQ_NNPDF31_TuneCP5_13TeV-powheg-pythia8 $targetmc18_dir/WWToLNuQQ  > WWToLNuQQ.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WZTo2L2Q_13TeV_amcatnloFXFX_madspin_pythia8 $targetmc18_dir/WZTo2L2Q  > WZTo2L2Q.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/WZTo3LNu_TuneCP5_13TeV-amcatnloFXFX-pythia8 $targetmc18_dir/WZTo3LNu  > WZTo3LNu.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centralmc18_dir/ZZTo2L2Q_13TeV_amcatnloFXFX_madspin_pythia8 $targetmc18_dir/ZZTo2L2Q  > ZZTo2L2Q.out

#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8 $targetmc17_dir/TTTo2L2Nu > TTTo2L2Nu17.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/TTToHadronic_TuneCP5_13TeV-powheg-pythia8 $targetmc17_dir/TTToHadronic > TTToHadronic17.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8 $targetmc17_dir/TTToSemiLeptonic > TTToSemiLeptonic17.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/WJetsToLNu_HT-100To200_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc17_dir/WJetsToLNu_HT-100To200 > WJetsToLNu_HT-100To20017.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/WJetsToLNu_HT-200To400_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc17_dir/WJetsToLNu_HT-200To400 > WJetsToLNu_HT-200To40017.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/WJetsToLNu_HT-400To600_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc17_dir/WJetsToLNu_HT-400To600 > WJetsToLNu_HT-400To60017.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/WJetsToLNu_HT-600To800_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc17_dir/WJetsToLNu_HT-600To800 > WJetsToLNu_HT-600To80017.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/WJetsToLNu_HT-800To1200_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc17_dir/WJetsToLNu_HT-800To1200 > WJetsToLNu_HT-800To120017.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/WJetsToLNu_HT-1200To2500_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc17_dir/WJetsToLNu_HT-1200To2500 > WJetsToLNu_HT-1200To250017.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/WJetsToLNu_HT-2500ToInf_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc17_dir/WJetsToLNu_HT-2500ToInf > WJetsToLNu_HT-2500ToInf17.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/DYJetsToLL_M-10to50_TuneCP5_13TeV-madgraphMLM-pythia8 $targetmc17_dir/DYJetsToLL_M-10to50 > DYJetsToLL_M-10to50.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8 $targetmc17_dir/DYJetsToLL_M-50-amcatnloFXFX > DYJetsToLL_M-50-amcatnloFXFX.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc17_dir/ST_t-channel_antitop > ST_t-channel_antitop.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/ST_t-channel_top_4f_InclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc17_dir/ST_t-channel_top > ST_t-channel_top.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/ST_tW_antitop_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8 $targetmc17_dir/ST_tW_antitop > ST_tW_antitop.out
#./skimnanoaod.py -F --split 27 -Y 2017 $centralmc17_dir/ST_tW_top_5f_inclusiveDecays_TuneCP5_13TeV-powheg-pythia8 $targetmc17_dir/ST_tW_top > ST_tW_top.out

#./skimnanoaod.py -F --split 27 -Y 2016CP5 $centralmc16_dir/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc16_dir/TTTo2L2Nu > TTTo2L2Nu16.out
#./skimnanoaod.py -F --split 27 -Y 2016CP5 $centralmc16_dir/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc16_dir/TTToHadronic > TTToHadronic16.out
#./skimnanoaod.py -F --split 27 -Y 2016CP5 $centralmc16_dir/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc16_dir/TTToSemiLeptonic > TTToSemiLeptonic16.out
#./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/WJetsToLNu_HT-100To200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8 $targetmc16_dir/WJetsToLNu_HT-100To200 > WJetsToLNu_HT-100To20016.out
#./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/WJetsToLNu_HT-200To400_TuneCUETP8M1_13TeV-madgraphMLM-pythia8 $targetmc16_dir/WJetsToLNu_HT-200To400 > WJetsToLNu_HT-200To40016.out
#./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/WJetsToLNu_HT-400To600_TuneCUETP8M1_13TeV-madgraphMLM-pythia8 $targetmc16_dir/WJetsToLNu_HT-400To600 > WJetsToLNu_HT-400To60016.out
#./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/WJetsToLNu_HT-600To800_TuneCUETP8M1_13TeV-madgraphMLM-pythia8 $targetmc16_dir/WJetsToLNu_HT-600To800 > WJetsToLNu_HT-600To80016.out
#./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/WJetsToLNu_HT-800To1200_TuneCUETP8M1_13TeV-madgraphMLM-pythia8 $targetmc16_dir/WJetsToLNu_HT-800To1200 > WJetsToLNu_HT-800To120016.out
./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/WJetsToLNu_HT-1200To2500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8 $targetmc16_dir/WJetsToLNu_HT-1200To2500 > WJetsToLNu_HT-1200To250016.out
./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/WJetsToLNu_HT-2500ToInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8 $targetmc16_dir/WJetsToLNu_HT-2500ToInf > WJetsToLNu_HT-2500ToInf16.out
./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/DYJetsToLL_M-10to50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8 $targetmc16_dir/DYJetsToLL_M-10to50 > DYJetsToLL_M-10to5016.out
./skimnanoaod.py -F --split 27 -Y 2016 $centralmc16_dir/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8 $targetmc16_dir/DYJetsToLL_M-50-amcatnloFXFX > DYJetsToLL_M-50-amcatnloFXFX16.out
./skimnanoaod.py -F --split 27 -Y 2016CP5 $centralmc16_dir/ST_t-channel_antitop_4f_InclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc16_dir/ST_t-channel_antitop > ST_t-channel_antitop16.out
./skimnanoaod.py -F --split 27 -Y 2016CP5 $centralmc16_dir/ST_t-channel_top_4f_InclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc16_dir/ST_t-channel_top > ST_t-channel_top16.out
./skimnanoaod.py -F --split 27 -Y 2016CP5 $centralmc16_dir/ST_tW_antitop_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc16_dir/ST_tW_antitop > ST_tW_antitop16.out
./skimnanoaod.py -F --split 27 -Y 2016CP5 $centralmc16_dir/ST_tW_top_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8 $targetmc16_dir/ST_tW_top > ST_tW_top16.out
