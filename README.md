**I. INTRODUCTION**

With this package, we can do the following to NanoAOD files
- Apply good JSON
- Calculate b-tag weights for MC
- Apply Jet/MET corrections
- skim events

For this, we use the data frame concept to simplify the code, but
it requires to learn new way of using ROOT.
In a data frame concept, the operations on data should be defined
in terms of functions, which means that result of applying
a function should produce outputs and no internal states are allowed.
In ROOT, RDataFrame allows us to borrow the concept for analysis.

This package code implements a hadronic channel analysis using NanoAOD format.
However, it can be adapted for any flat ROOT trees.

Separate branches exist for 2017 samples and 2018 samples.

- The advantage of using RDataFrame is that you don't have to
worry about looping, filling of histograms at the right place,
writing out trees. In some sense you can concentrate more on
thinking about data analysis.

- Draw back is that you have to think of a new way of doing old things.
This could getting used to. Alos complicated algorithms may not
be so easy to implement with RDataFrame

- Purely data frame concept is not ideal for HEP data since
we have to treat real data and simulated data differently.
We must apply different operations depending on the input data,
   such as scale factors and corrections.
Therefore, some form of global state should be stored. 
In this package, the data frame concept is used together
with object oriented concept for this purpose.


**II. Code**

- The code consists of a main class NanoAODAnalyzerrdframe. 
There is one .h header file and one .cpp source file for it.
The class has several methods:
    - Object definitions (selectElectrons, selectMuons, ...)
    - Additional derived variables definition (defineMoreVars)
    - Histogram definitions (bookHists)
    - Selections (defineCuts)
    - Plus some utility methods/functions (gen4vec, readjson, removeOverlaps, helper_1DHistCreator, createHists)

- Users should modify: object definitions, define additional variables, histogram definitions, selections.

- To use the class, you need to pass a pointer to TChain object, output root file name and a JSON file.
  If there is "genWeight" branch in the ROOT Tree then the input is assumed to be MC, otherwise data.
  Look at nanoaoddataframe.cpp to find how to use within C++ 

**III. Clone Repository in lyoui**

  > git clone  https://gitlab.cern.ch/cdozen/nanoaodrdtool
	
  > cd nanoaodrdtool/

- **Compiling** 

  Need ROOT 6.24 or later (available in /gridgroup/cms/cdozen/NanoAOD/root6.24/ in our lyoserv server) then,

  > source setuproot6_24.sh

  this will compile and create a libnanoaodrdframe.so shared library that can be loaded in a ROOT session or macro:
  gSystem->Load("libnanoadrdframe.so");

  or within pyROOT (look in processnanoaod.py).

  then, 
  > make

**III. Running over large dataset**

As an example we used TprimeBToTH NanoAOD dataset.

_Input Dataset info:_ 
*******************************************************************************************************
|RunIIFall17NanoAODv7 | Parent MC 2017MiniAOD |
| ------ | ------ |
| RunIIFall17NanoAODv7: [DAS](https://cmsweb.cern.ch/das/request?instance=prod/global&input=file+dataset%3D%2FTprimeBToTH_M-1000_LH_TuneCP5_13TeV-madgraph-pythia8%2FRunIIFall17NanoAODv7-PU2017_12Apr2018_Nano02Apr2020_102X_mc2017_realistic_v8-v1%2FNANOAODSIM) | RunIIFall17MiniAODv2 :[DAS](https://cmsweb.cern.ch/das/request?view=list&limit=50&instance=prod%2Fglobal&input=file+dataset%3D%2FTprimeBToTH_M-1000_LH_TuneCP5_13TeV-madgraph-pythia8%2FRunIIFall17MiniAODv2-PU2017_12Apr2018_94X_mc2017_realistic_v14-v1%2FMINIAODSIM)|
*******************************************************************************************************

- **Skim NanoAOD**

  - **_Usage_:**  skimnanoad.py script goes into subdirectories recursively and process ROOT files.  It will create one output file per one input file, so there is a one-to-one correspondence.

    > ./skimnanoaod.py [options] {$inputDir} {$outputDir}

      - **_{$inputDir}_** : MC-RUNIIFall17NanoAODv7 root files stored in gridgroup-cms area. They will be used as input files:
        > /gridgroup/cms/cdozen/NanoAOD/mc/RunIIFall17NanoAODv7/TprimeBToTH_M-1000_LH_TuneCP5_13TeV-madgraph-pythia8/NANOAODSIM/

      - **_{$outputDir}_**: create your own output directory to store the skimming output root files ! For instance:
        > mkdir outputskimfiles

      - **_[options]_**:
      
        --split : How many jobs to split into. it will allow multithreading you to use multiple CPUs to process the files. (e.g. --split=5)

	      --json : Select events using this JSON file, meaningful only for data 

	      --skipold : will let you skip files that were analyzed, by matching files that have _analyzed in the file names

	      --recursive : Process files in the subdirectories recursively. Recursive mode is "on" by default. If you don't want that, --recursive=False.

	      --allinone  : will let you get a single output root file for all the files in the inputDir. You must make sure MC and Data are not mixed together. In this mode, --skipold doesn't work

	      --saveallbranches : to save all branches. Default is to save only select branches

	      --globaltag : Global tag information, which is used for JetMET corrections. If omitted, then no corrections are applied.

	      -Y : year option: 2016/2017/2018 


  - **_Run_**:
      > ./skimnanoaod.py --split 10  -Y 2017  /gridgroup/cms/cdozen/NanoAOD/mc/RunIIFall17NanoAODv7/TprimeBToTH_M-1000_LH_TuneCP5_13TeV-madgraph-pythia8/NANOAODSIM/ outputskimfiles >TprimeNanoAODskim.out


All files skimmed and stored under the outputskimfiles directory.

- **Analyse skimNanoAOD files (process NanoAOD):**

    - **_Usage_**: processnanoaod.py script can automatically run over all ROOT files in an input directory. Take a look at skimevents.sh script to see how it's done.
      
      > ./processnanoaod.py  [options] {$inputDir} {$outputroot}

      - {$inputDir}: The skimmed root files in "outputskimfiles" directory 

      - {$outputroot}: Give a outroot file name. e.g: "Tprime_analyzed.root" 
  
      - [options] : as explained above.

    - **_Run_**:
      > ./processnanoaod.py  --allinone -Y 2017  outputskimfiles/ Tprime_analyzed.root > processnano.out 



