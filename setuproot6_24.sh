#!/bin/bash

#############################  compiling nanoaodframe  ######################################

## 	Need ROOT 6.24  : available in /gridgroup/cms/cdozen/NanoAOD/root6.24/ in lyoserv) 
## 	To setup root6.24 run this script :  "./setuproot6_24.sh"

#############################################################################################


# load thisroot.sh function file for root6.24

source /gridgroup/cms/cdozen/NanoAOD/root6.24/root/bin/thisroot.sh

echo "Hello, I am root6.24"
echo "I'm rooting for you to compile NanoAOD rd frame tool for T' analyis"
echo "Good Luck on your upcoming show :)"
