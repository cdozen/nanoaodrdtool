#!/bin/bash
centraldata_dir="/data/common/NanoAOD/data"
centralmc16_dir="/data/common/NanoAOD/mc"
centralmc17_dir="/data/common/NanoAOD/mc"
centralmc18_dir="/data/common/NanoAOD/mc/RunIIAutumn18NanoAODv6"

targetmc18_dir="/data1/common/skimmed_NanoAOD/skim_NDFv3_6_0/mc/2018"
targetdata_dir="/data1/common/skimmed_NanoAOD/skim_NDFv3_6_0/data/"

#./skimnanoaod.py -F --split 25 -Y 2018 $centraldata_dir/Run2018A/SingleMuon/NANOAOD/Nano25Oct2019-v1 $targetdata_dir/SingleMuon2018A > SingleMuon2018A.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centraldata_dir/Run2018B/SingleMuon/NANOAOD/Nano25Oct2019-v1 $targetdata_dir/SingleMuon2018B > SingleMuon2018B.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centraldata_dir/Run2018C/SingleMuon/NANOAOD/Nano25Oct2019-v1 $targetdata_dir/SingleMuon2018C > SingleMuon2018C.out
#./skimnanoaod.py -F --split 25 -Y 2018 $centraldata_dir/Run2018D/SingleMuon/NANOAOD/Nano25Oct2019-v1 $targetdata_dir/SingleMuon2018D > SingleMuon2018D.out

./skimnanoaod.py -F --split 27 -Y 2016 $centraldata_dir/Run2016B_ver2 $targetdata_dir/SingleMuon2016B_ver2 > SingleMuon2016B_ver2.out
./skimnanoaod.py -F --split 27 -Y 2016 $centraldata_dir/Run2016C $targetdata_dir/SingleMuon2016C > SingleMuon2016C.out
./skimnanoaod.py -F --split 27 -Y 2016 $centraldata_dir/Run2016D $targetdata_dir/SingleMuon2016D > SingleMuon2016D.out
./skimnanoaod.py -F --split 27 -Y 2016 $centraldata_dir/Run2016E $targetdata_dir/SingleMuon2016E > SingleMuon2016E.out
./skimnanoaod.py -F --split 27 -Y 2016 $centraldata_dir/Run2016F $targetdata_dir/SingleMuon2016F > SingleMuon2016F.out
./skimnanoaod.py -F --split 27 -Y 2016 $centraldata_dir/Run2016G $targetdata_dir/SingleMuon2016G > SingleMuon2016G.out
./skimnanoaod.py -F --split 27 -Y 2016 $centraldata_dir/Run2016H $targetdata_dir/SingleMuon2016H > SingleMuon2016H.out
./skimnanoaod.py -F --split 27 -Y 2017 $centraldata_dir/Run2017B $targetdata_dir/SingleMuon2017B > SingleMuon2017B.out
./skimnanoaod.py -F --split 27 -Y 2017 $centraldata_dir/Run2017C $targetdata_dir/SingleMuon2017C > SingleMuon2017C.out
./skimnanoaod.py -F --split 27 -Y 2017 $centraldata_dir/Run2017D $targetdata_dir/SingleMuon2017D > SingleMuon2017D.out
./skimnanoaod.py -F --split 27 -Y 2017 $centraldata_dir/Run2017E $targetdata_dir/SingleMuon2017E > SingleMuon2017E.out
./skimnanoaod.py -F --split 27 -Y 2017 $centraldata_dir/Run2017F $targetdata_dir/SingleMuon2017F > SingleMuon2017F.out
