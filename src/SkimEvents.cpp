/*
 * SkimEvents.cpp
 *
 *  Created on: Dec 9, 2018
 *      Author: suyong
 */

#include "SkimEvents.h"
#include "utility.h"

SkimEvents::SkimEvents(TTree *t, std::string outfilename, std::string year, std::string jsonfname, string globaltag, int nthreads)
:NanoAODAnalyzerrdframe(t, outfilename, year, jsonfname, globaltag, nthreads),_year(year)
{

}

// Define your cuts here
void SkimEvents::defineCuts()
{
	// Cuts to be applied in order
	// These will be passed to Filter method of RDF
	// check for good json event is defined earlier
        std::cout<<"YEAR "<<_year<<std::endl;
        bool isWJetincl = false;
        if(isWJetincl){
                addCuts("LHE_HT <= 100","0");
                if(_year.find("2016") != std::string::npos){
                        addCuts("(HLT_IsoMu24 || HLT_IsoTkMu24) && nmuonpass == 1 && nvetoelepass == 0 && nvetomuons == 0","00");
                }
                else if(_year == "2017"){
                        addCuts("HLT_IsoMu27 && nmuonpass == 1 && nvetoelepass == 0 && nvetomuons == 0","00");
                }
                else if(_year == "2018"){
                        std::cout<<"2018 cut"<<std::endl;
                        addCuts("HLT_IsoMu24 && nmuonpass == 1 && nvetoelepass == 0 && nvetomuons == 0","00");
                }
        }
        else{ 
                if(_year.find("2016") != std::string::npos){
                        addCuts("(HLT_IsoMu24 || HLT_IsoTkMu24) && nmuonpass == 1 && nvetoelepass == 0 && nvetomuons == 0","0");
                }
                else if(_year == "2017"){
                        addCuts("HLT_IsoMu27 && nmuonpass == 1 && nvetoelepass == 0 && nvetomuons == 0","0");
                }
                else if(_year == "2018"){
                        std::cout<<"2018 cut"<<std::endl;
                        addCuts("HLT_IsoMu24 && nmuonpass == 1 && nvetoelepass == 0 && nvetomuons == 0","0");
                }
        }
        //addCuts("nMuon>=1","0");
}

void SkimEvents::defineMoreVars()
{
	addVar({"Sel_jetHT", "Sum(Sel_jetpt)"});

	// define variables that you want to store
	addVartoStore("run");
	addVartoStore("luminosityBlock");
	addVartoStore("event");
	addVartoStore("evWeight.*");
	addVartoStore("puWeight.*");
	addVartoStore("pugenWeight.*");
	addVartoStore("gen.*");
	addVartoStore("Pileup.*");
	addVartoStore("btagWeight.*");
        //addVartoStore("HLT_.*");

	addVartoStore("nJet");
	addVartoStore("Jet_.*");
        addVartoStore("nTau");
        addVartoStore("Tau_.*");
	addVartoStore("MET.*");
	addVartoStore("LHE_.*");
	addVartoStore("Puppi.*");
	addVartoStore("nElectron");
	addVartoStore("Electron_.*");
	addVartoStore("nMuon");
	addVartoStore("Muon_.*");
	addVartoStore("PV.*");
	addVartoStore("nOtherPV");
	addVartoStore("OtherPV_z");
	addVartoStore("Flag.*");
}

void SkimEvents::bookHists()
//void SkimEvents::bookHists()
{
	// _hist1dinfovector contains the information of histogram definitions (as TH1DModel)
	// the variable to be used for filling
	// and the minimum cutstep for which the histogram should be filled
	//
	// The braces are used to initalize the struct
	// TH1D
        add1DHist( {"hcounter1", "Event counter", 2, -0.5, 1.5}, "one", "one", "");
        add1DHist( {"hcounter2", "Event counter", 2, -0.5, 1.5}, "one", "pugenWeight", "");
        add1DHist( {"hcounter3", "Event counter", 2, -0.5, 1.5}, "one", "evWeight", "");
        //add1DHist( {"hcounter", "Event counter", 2, -0.5, 1.5}, "one", "genWeight", "");
        //add1DHist( {"hcounter2", "Event counter", 2, -0.5, 1.5}, "one", "pugenWeight", "");
}
