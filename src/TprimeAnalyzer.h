/*
 * TprimeAnalyzer.h
 *
 *  Created on: Dec 4, 2018
 *      Author: suyong
 */

#ifndef TprimeANALYZER_H_
#define TprimeANALYZER_H_

#include "NanoAODAnalyzerrdframe.h"

class TprimeAnalyzer: public NanoAODAnalyzerrdframe
{
	public:
		TprimeAnalyzer(TTree *t, std::string outfilename, std::string year="", std::string jsonfname="", string globaltag="", int nthreads=1);
		void defineCuts();
		void defineMoreVars(); // define higher-level variables from
		void bookHists();

};



#endif /* TprimeANALYZER_H_ */
